const { ObjectId } = require("mongodb");
const { getCollectionByTitle } = require("../Schemas/db");
const { PackageVersionSchema } = require("../Schemas");
const { getRecordsIdProb, getRecordDates } = require("../Controller");

const collectionTitle = "packageVersion";

const addPackageVersion = async (packageVersion) => {
  const validation = PackageVersionSchema.validate(packageVersion);
  if (validation.error) {
    console.log(validation.error);
    return { success: false, message: "Missing Data", result: null };
  }

  const collection = getCollectionByTitle(collectionTitle);
  await collection.insertOne({ ...packageVersion, ...getRecordDates() });
  return { success: true, message: "Package Version is added", result: null };
};

const getPackageVersionByPackageId = (packageId) => {
  const collection = getCollectionByTitle(collectionTitle);
  return collection.find({}).toArray();
};

const getPackageVersions = async () => {
  const collection = await getCollectionByTitle(collectionTitle);
  const result = getRecordsIdProb(await collection.find({}).toArray());
  return { success: true, result };
};

module.exports = {
  addPackageVersion,
  getPackageVersionByPackageId,
  getPackageVersions,
};
