const { ObjectId } = require("mongodb");
const { getCollectionByTitle } = require("../Schemas/db");
const { PackageSchema } = require("../Schemas");
const { getRecordsIdProb, getRecordDates } = require("../Controller");
const { addPackageVersion } = require("./PackageVersions");
const collectionTitle = "package";

const addPackage = async (package, packageVersion) => {
  const validation = PackageSchema.validate(package);
  if (validation.error) {
    console.log(validation.error);
    return { success: false, message: "Missing Data", result: null };
  }

  const collection = getCollectionByTitle(collectionTitle);
  const insertedPackage = await collection.insertOne({
    ...package,
    ...getRecordDates(),
  });
  const packageId = insertedPackage.insertedId.toString();

  await addPackageVersion({ ...packageVersion, packageId });
  return { success: true, message: "Package is added", result: packageId };
};

const getPackages = async () => {
  const collection = await getCollectionByTitle(collectionTitle);
  const result = getRecordsIdProb(await collection.find({}).toArray());
  return { success: true, result };
};

const updatePackageById = (id, quantity) => {
  const collection = getCollectionByTitle(collectionTitle);
  return collection.updateOne({ _id: ObjectId(id) }, { $inc: { quantity } });
};

module.exports = { addPackage, getPackages, updatePackageById };
