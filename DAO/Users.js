const { ObjectId } = require("mongodb");
const { getCollectionByTitle } = require("../Schemas/db");
const { UserSchema, SignInUserSchema } = require("../Schemas");
const {
  getRecordsIdProb,
  getRecordIdProb,
  getRecordDates,
} = require("../Controller");

const collectionTitle = "user";

const addUser = async (user) => {
  const validation = UserSchema.validate(user);
  if (validation.error) {
    console.log(validation.error);
    return { success: false, message: "Missing Data", result: null };
  }

  const collection = getCollectionByTitle(collectionTitle);

  const insertedUser = await collection.insertOne({
    ...user,
    ...getRecordDates(),
  });
  const userId = insertedUser.insertedId.toString();

  return {
    success: true,
    message: "User is added",
    result: { id: userId, ...user },
  };
};

const getUsers = async () => {
  const collection = await getCollectionByTitle(collectionTitle);
  const result = getRecordsIdProb(await collection.find({}).toArray());
  return { success: true, result };
};

const getUserByEmailAndPassword = async (user) => {
  const validation = SignInUserSchema.validate(user);
  if (validation.error) {
    console.log(validation.error);
    return { success: false, message: "Missing Data", result: null };
  }
  const collection = getCollectionByTitle(collectionTitle);
  const { password, ...result } = await collection.findOne({
    email: user.email,
  });
  return { success: true, result: getRecordIdProb(result) };
};

const updateUser = (id, quantity) => {
  const collection = getCollectionByTitle(collectionTitle);
  return collection.updateOne({ _id: ObjectId(id) }, { $inc: { quantity } });
};

module.exports = { addUser, getUsers, updateUser, getUserByEmailAndPassword };
