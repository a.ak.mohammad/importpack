const express = require("express");
const path = require("path");
const { v4: uuidv4 } = require("uuid");

const { handleResult, getTempFolderPath } = require("../Controller");
const router = express.Router();

router.post("/upload", async (req, res) => {
  try {
    console.log(req.body);
    let ETL = req.body.ETL;
    let lang = req.body.lang ? req.body.lang : "ara";
    let tempFile = req.files.file;

    const uploadedFile = path.join(
      getTempFolderPath(),
      `${uuidv4()}${path.extname(tempFile.name)}`
    );

    await tempFile.mv(uploadedFile);

    res.status(200).json({ results: "Success" }).end();
  } catch (err) {
    console.error(err);
    res.status(200).json({ results: "Network Problem" }).end();
  }
});

module.exports = router;
