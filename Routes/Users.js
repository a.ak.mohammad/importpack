const express = require("express");
const {
  addUser,
  getUsers,
  getUserByEmailAndPassword,
} = require("../DAO/Users");
const { handleResult } = require("../Controller");
const router = express.Router();

router.post("/signin", (req, res) => {
  const user = req.body;
  handleResult(() => getUserByEmailAndPassword(user), res);
});

router.post("/", (req, res) => {
  const user = req.body;
  handleResult(async () => addUser(user), res);
});

router.get("/", (req, res) => {
  handleResult(async () => getUsers(), res);
});

module.exports = router;
