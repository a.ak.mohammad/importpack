const express = require("express");
const {
  addPackage,
  getPackages,
  updatePackageById,
} = require("../DAO/Packages");
const { handleResult } = require("../Controller");

const router = express.Router();

router.post("/", (req, res) => {
  const { package, packageVersion } = req.body;
  handleResult(async () => addPackage(package, packageVersion), res);
});

router.get("/", (req, res) => {
  handleResult(async () => getPackages(), res);
});

module.exports = router;
