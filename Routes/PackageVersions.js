const express = require("express");
const {
  addPackageVersion,
  getPackageVersions,
  getPackageVersionByPackageId,
} = require("../DAO/PackageVersions");
const { handleResult } = require("../Controller");

const router = express.Router();

router.post("/", (req, res) => {
  const packageVersion = req.body;
  handleResult(async () => addPackageVersion(packageVersion), res);
});

router.get("/", (req, res) => {
  handleResult(async () => getPackageVersions(), res);
});

module.exports = router;
