const fs = require("fs");
const path = require("path");

// directory to check if exists
const dir = "./uploads";

// check if directory exists
if (fs.existsSync(dir)) {
  console.log("Directory exists!");
} else {
  console.log("Directory not found.");
}

const getMainFolderPath = () => {
  const uploadFolder = path.join(__dirname, "..", "packages");
  if (!fs.existsSync(uploadFolder)) fs.mkdirSync(uploadFolder);
  return uploadFolder;
};

const getTempFolderPath = () => {
  const mainFolder = getMainFolderPath();
  const tempFolderPath = path.join(mainFolder, "temp");
  if (!fs.existsSync(tempFolderPath))
    fs.mkdirSync(tempFolderPath, {
      recursive: true,
    });
  return tempFolderPath;
};

const getCurrentPackageFolderPath = (language, packageName, packageVersion) => {
  const mainFolder = getMainFolderPath();
  const packageFolderPath = path.join(
    mainFolder,
    language,
    packageName,
    packageVersion
  );
  if (!fs.existsSync(packageFolderPath))
    fs.mkdirSync(packageFolderPath, {
      recursive: true,
    });
  return packageFolderPath;
};

const handleResult = async (func, res) => {
  try {
    const result = await func();
    res
      .status(200)
      .json({ code: 200, ...result })
      .end();
  } catch (err) {
    console.error(err);
    res.status(200).json({ code: 400, result: null }).end();
  }
};

const getRecordsIdProb = (arr) => {
  if (!arr || !Array.isArray(arr)) return [];
  let newArr = arr.map((obj) => {
    return getRecordIdProb(obj);
  });
  return newArr;
};

const getRecordIdProb = (obj) => {
  if (!obj || typeof obj !== "object") return {};
  let newObj = { id: obj._id, ...obj };
  delete newObj._id;
  return newObj;
};

const getRecordDates = () => {
  let now = new Date();
  now = now.toISOString();
  return { createdAt: now, updatedAt: now };
};
module.exports = {
  handleResult,
  getRecordsIdProb,
  getRecordIdProb,
  getMainFolderPath,
  getTempFolderPath,
  getCurrentPackageFolderPath,
  getRecordDates,
};
