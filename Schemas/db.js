const { MongoClient, ObjectId } = require("mongodb");

// const connectionUrl = "mongodb://localhost:27017";
const connectionUrl =
  "mongodb+srv://aakmohammad:eli992@cluster0.vgjq4.mongodb.net/importPackDB?retryWrites=true&w=majority";

const dbName = "importPackDB";

let db;

const init = () =>
  MongoClient.connect(connectionUrl, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
    .then((client) => {
      db = client.db(dbName);
      console.log("DB connected");
    })
    .catch((err) => {
      console.error(err);
      console.error("DB not connected");
    });

// init();

const getCollectionByTitle = (collectionTitle) =>
  db.collection(collectionTitle);

module.exports = { init, getCollectionByTitle };
