const Joi = require("@hapi/joi");

const UserSchema = Joi.object().keys({
  username: Joi.string(),
  firstName: Joi.string(),
  lastName: Joi.string(),
  email: Joi.string(),
  phone: Joi.string(),
  password: Joi.string(),
  isBanned: [true, false],
  isAdmin: [true, false],
});

const SignInUserSchema = Joi.object().keys({
  email: Joi.string(),
  password: Joi.string(),
});

const PackageSchema = Joi.object().keys({
  maintainer: Joi.string(),
  language: Joi.string(),
  packageName: Joi.string(),
  filePath: Joi.string(),
});

const PackageVersionSchema = Joi.object().keys({
  packageId: Joi.string(),
  code: Joi.string(),
  // rating: Joi.string(),
  discreption: Joi.string(),
  dependecines: Joi.array().items(Joi.string()),
  isApproved: [true, false],
});

module.exports = {
  UserSchema,
  SignInUserSchema,
  PackageSchema,
  PackageVersionSchema,
};
