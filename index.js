const { init } = require("./Schemas/db");
const express = require("express");
const cors = require("cors");
const fileUpload = require("express-fileupload");
const bodyParser = require("body-parser");

const FilesRouter = require("./Routes/Files");
const UsersRoter = require("./Routes/Users");
const PackagesRoter = require("./Routes/Packages");
const PackageVersionsRoter = require("./Routes/PackageVersions");

const apiVersion = "/api/v1";
const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(
  fileUpload({
    createParentPath: true,
    limits: {
      fileSize: 4 * 2048 * 2048 * 2048, //8MB max file(s) size
    },
  })
);
app.use(express.static("./packages"));

// routes

app.use(`${apiVersion}/files`, FilesRouter);
app.use(`${apiVersion}/users`, UsersRoter);
app.use(`${apiVersion}/packages`, PackagesRoter);
app.use(`${apiVersion}/packageVersions`, PackageVersionsRoter);

init().then(() =>
  app.listen(7000, () => console.log("starting server on port 3000"))
);
